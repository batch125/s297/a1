let http = require("http");

let port = 3000;

http.createServer((request,response)=>
	{
		//uri/endpoint = resource
		//http method
		//body

		console.log(request);

		if (request.url === "/profile" && request.method === "GET"){
		
				response.writeHead(200,{"content-Type": "text/html"}); 	
				response.end(`Welcome to my page`);
			}

		else if (request.url === "/profile" && request.method === "POST"){
		
				response.writeHead(200,{"content-Type": "text/html"}); 	
				response.end(`Data to be sent to database`);
			}
		else {
			response.writeHead(404,{"content-Type": "text/html"}); 	
			response.end(`Request can not be completed`);
		}
	}).listen(port);

console.log(`Server is now conected to port ${port}`);